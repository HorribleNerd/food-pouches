package com.horriblenerd.foodpouches;

import net.minecraftforge.common.ForgeConfigSpec;

public class Config {
    public static final String CATEGORY_GENERAL = "general";
    public static final String CATEGORY_POUCHES = "pouches";

    public static ForgeConfigSpec COMMON_CONFIG;
    public static ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.BooleanValue AUTO_ENABLED;
    public static ForgeConfigSpec.BooleanValue CREATIVE_ENABLED;

    public static ForgeConfigSpec.BooleanValue LONG_USE_TIME;
    public static ForgeConfigSpec.BooleanValue ADVANCED_TOOLTIP;

    public static ForgeConfigSpec.IntValue SMALL_SIZE;
    public static ForgeConfigSpec.IntValue MEDIUM_SIZE;
    public static ForgeConfigSpec.IntValue LARGE_SIZE;
    public static ForgeConfigSpec.IntValue MASSIVE_SIZE;

    static {
        ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();
        ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

        COMMON_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        AUTO_ENABLED = COMMON_BUILDER.comment("Enable automatic pouch recipes").define("auto_enabled", true);
        CREATIVE_ENABLED = COMMON_BUILDER.comment("Enable creative pouch recipe").define("creative_enabled", false);
        LONG_USE_TIME = COMMON_BUILDER.comment("Enable long use time (48 ticks instead of 32 ticks)").define("long_use_time", true);
        COMMON_BUILDER.comment("Pouch settings", "Sizes in meals (1 full hunger bar)").push(CATEGORY_POUCHES);
        SMALL_SIZE = COMMON_BUILDER.defineInRange("small_size", 25, 0, Integer.MAX_VALUE);
        MEDIUM_SIZE = COMMON_BUILDER.defineInRange("medium_size", 50, 0, Integer.MAX_VALUE);
        LARGE_SIZE = COMMON_BUILDER.defineInRange("large_size", 100, 0, Integer.MAX_VALUE);
        MASSIVE_SIZE = COMMON_BUILDER.defineInRange("massive_size", 500, 1, Integer.MAX_VALUE);

        CLIENT_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        ADVANCED_TOOLTIP = CLIENT_BUILDER.comment("Enable advanced tooltip").define("advanced_tooltip", true);


        COMMON_BUILDER.pop();
        CLIENT_BUILDER.pop();
        COMMON_CONFIG = COMMON_BUILDER.build();
        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

}
