package com.horriblenerd.foodpouches;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.List;

public class FoodPouchItem extends Item {

    private final boolean automatic;
    private final boolean infinite;
    private int capacity = 200;

    public FoodPouchItem() {
        super(new Item.Properties().group(ItemGroup.FOOD).maxStackSize(1));
        automatic = false;
        infinite = false;
    }

    public FoodPouchItem(boolean auto) {
        super(new Item.Properties().group(ItemGroup.FOOD).maxStackSize(1));
        automatic = auto;
        infinite = false;
    }

    public FoodPouchItem(boolean auto, boolean infinite) {
        super(new Item.Properties().group(ItemGroup.FOOD).maxStackSize(1));
        automatic = auto;
        this.infinite = infinite;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        if (playerIn.isSneaking()) {
            if (this.isInfinite()) {
                itemstack.setDamage(0);
                this.setFoodLevel(itemstack, Config.MASSIVE_SIZE.get());
                return ActionResult.resultFail(itemstack);
            }
            float food = 0;
            PlayerInventory inv = playerIn.inventory;
            for (ItemStack i : inv.mainInventory) {
                if (i.isFood()) {
                    Food f = i.getItem().getFood();
                    if (f != null && f.getEffects().isEmpty()) {
                        float amount = (f.getSaturation() * 2.0F) * f.getHealing();
                        float count;

                        int remaining = itemstack.getDamage();
                        if (amount * i.getCount() > remaining) {
                            count = (remaining / (amount));
                        } else
                            count = i.getCount();
                        i.shrink((int) Math.ceil(count));
                        food += count * amount;
                        addFoodLevel(itemstack, (int) food);
                    }
                }
            }
            return ActionResult.resultSuccess(itemstack);
        } else if (!this.isAutomatic() && playerIn.canEat(false) && itemstack.getDamage() + 1 < itemstack.getMaxDamage()) {
            playerIn.setActiveHand(handIn);
            return ActionResult.resultConsume(itemstack);
        }
        return ActionResult.resultFail(itemstack);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving) {
        if (!(entityLiving instanceof PlayerEntity))
            return stack;
        PlayerEntity player = (PlayerEntity) entityLiving;

        int needed = 20 - player.getFoodStats().getFoodLevel();
        if (isInfinite()) {
            player.getFoodStats().addStats(needed, 20);
            stack.setDamage(0);
        } else {
            int able = getFoodLevel(stack);
            int restored = (able < 20 ? able : needed);
            player.getFoodStats().addStats(restored, restored);

            stack.setDamage(stack.getDamage() + restored);
        }
        return stack;
    }

    @Override
    public SoundEvent getEatSound() {
        return SoundEvents.ENTITY_GENERIC_EAT;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return Config.LONG_USE_TIME.get() ? 48 : 32;
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (this.automatic && entityIn instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) entityIn;
            onItemUseFinish(stack, worldIn, player);
        }
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        StringTextComponent meals;
        if (isInfinite()) {
            meals = new StringTextComponent("");
            meals.mergeStyle(TextFormatting.GREEN);
            meals.append(new StringTextComponent("999999 ").mergeStyle(TextFormatting.OBFUSCATED));
            meals.append(new StringTextComponent("meals"));
        } else {
            meals = new StringTextComponent(String.format("%d meals", getFoodLevel(stack) / 20));
            meals.mergeStyle(TextFormatting.GREEN);
        }
        tooltip.add(meals);
        if ((isInfinite() && !Config.CREATIVE_ENABLED.get()) || (isAutomatic() && !Config.AUTO_ENABLED.get())) {
            StringTextComponent enabled = new StringTextComponent("");
            enabled.append(new StringTextComponent("Recipe Disabled!").mergeStyle(TextFormatting.RED));
            tooltip.add(enabled);
        }
        if (Config.ADVANCED_TOOLTIP.get()) {
            if (Screen.hasShiftDown()) {
                StringTextComponent advanced = new StringTextComponent("");
                advanced.mergeStyle(TextFormatting.BLUE);
                advanced.append(new StringTextComponent("Sneak + Use while holding to fill"));
                advanced.append(new StringTextComponent("\nEating only depletes it by the required amount"));
                advanced.append(new StringTextComponent("\n1 meal = 20 hunger/saturation (1 full hunger bar)"));
                tooltip.add(advanced);
            } else {
                StringTextComponent advanced = new StringTextComponent("");
                advanced.append(new StringTextComponent("<press shift>"));
                advanced.mergeStyle(TextFormatting.BLUE);
                tooltip.add(advanced);
            }
        }

        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return this.infinite;
    }

    @Override
    public Rarity getRarity(ItemStack stack) {
        if (this.isInfinite())
            return Rarity.EPIC;
        return super.getRarity(stack);
    }

    @Override
    public int getMaxDamage(ItemStack stack) {
        return this.capacity;
    }

    public void setMealCapacity(int capacity) {
        this.capacity = capacity * 20;
    }

    public void setPreciseCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isInfinite() {
        return this.infinite;
    }

    public boolean isAutomatic() {
        return this.automatic;
    }

    public int getFoodLevel(ItemStack stack) {
        return this.infinite ? Integer.MAX_VALUE : getMaxDamage(stack) - getDamage(stack);
    }

    public void setFoodLevel(ItemStack stack, int foodLevel) {
        setDamage(stack, getMaxDamage(stack) - foodLevel);
    }

    public void addFoodLevel(ItemStack stack, int foodLevel) {
        setFoodLevel(stack, getFoodLevel(stack) + foodLevel);
    }

    @Override
    public boolean isRepairable(ItemStack stack) {
        return false;
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, PlayerEntity playerIn) {
        if (stack.getItem() instanceof FoodPouchItem) {
            if (((FoodPouchItem) stack.getItem()).isInfinite())
                stack.setDamage(0);
        }
        super.onCreated(stack, worldIn, playerIn);
    }
}
