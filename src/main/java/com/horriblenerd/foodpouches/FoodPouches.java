package com.horriblenerd.foodpouches;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(FoodPouches.MODID)
public class FoodPouches {
    // Directly reference a log4j logger.
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MODID = "foodpouches";

    public FoodPouches() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.COMMON_CONFIG);
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
        Registration.init();
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initSizes);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void initSizes(final FMLCommonSetupEvent event) {
        LOGGER.info("Initializing pouch sizes...");
        Registration.SMALL_POUCH.get().setMealCapacity(Config.SMALL_SIZE.get());
        Registration.MEDIUM_POUCH.get().setMealCapacity(Config.MEDIUM_SIZE.get());
        Registration.LARGE_POUCH.get().setMealCapacity(Config.LARGE_SIZE.get());
        Registration.MASSIVE_POUCH.get().setMealCapacity(Config.MASSIVE_SIZE.get());

        Registration.SMALL_AUTOMATIC_POUCH.get().setMealCapacity(Config.SMALL_SIZE.get());
        Registration.MEDIUM_AUTOMATIC_POUCH.get().setMealCapacity(Config.MEDIUM_SIZE.get());
        Registration.LARGE_AUTOMATIC_POUCH.get().setMealCapacity(Config.LARGE_SIZE.get());
        Registration.MASSIVE_AUTOMATIC_POUCH.get().setMealCapacity(Config.MASSIVE_SIZE.get());

        Registration.CREATIVE_POUCH.get().setMealCapacity(Config.MASSIVE_SIZE.get());
        LOGGER.info("Done.");
    }

}
