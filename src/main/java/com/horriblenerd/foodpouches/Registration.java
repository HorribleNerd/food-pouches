package com.horriblenerd.foodpouches;

import com.horriblenerd.foodpouches.recipes.PouchRecipeCraft;
import com.horriblenerd.foodpouches.recipes.PouchRecipeRepair;
import com.horriblenerd.foodpouches.recipes.PouchRecipeUpgrade;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class Registration {

    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, FoodPouches.MODID);
    private static final DeferredRegister<IRecipeSerializer<?>> RECIPES = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, FoodPouches.MODID);


    public static final RegistryObject<FoodPouchItem> SMALL_POUCH = ITEMS.register("small_food_pouch", FoodPouchItem::new);
    public static final RegistryObject<FoodPouchItem> MEDIUM_POUCH = ITEMS.register("medium_food_pouch", FoodPouchItem::new);
    public static final RegistryObject<FoodPouchItem> LARGE_POUCH = ITEMS.register("large_food_pouch", FoodPouchItem::new);
    public static final RegistryObject<FoodPouchItem> MASSIVE_POUCH = ITEMS.register("massive_food_pouch", FoodPouchItem::new);

    public static final RegistryObject<FoodPouchItem> SMALL_AUTOMATIC_POUCH = ITEMS.register("small_automatic_food_pouch", () -> new FoodPouchItem(true));
    public static final RegistryObject<FoodPouchItem> MEDIUM_AUTOMATIC_POUCH = ITEMS.register("medium_automatic_food_pouch", () -> new FoodPouchItem(true));
    public static final RegistryObject<FoodPouchItem> LARGE_AUTOMATIC_POUCH = ITEMS.register("large_automatic_food_pouch", () -> new FoodPouchItem(true));
    public static final RegistryObject<FoodPouchItem> MASSIVE_AUTOMATIC_POUCH = ITEMS.register("massive_automatic_food_pouch", () -> new FoodPouchItem(true));

    public static final RegistryObject<FoodPouchItem> CREATIVE_POUCH = ITEMS.register("creative_food_pouch", () -> new FoodPouchItem(true, true));

    public static final RegistryObject<PouchRecipeCraft.Serializer> CRAFTING_POUCH = RECIPES.register("pouch_crafting", PouchRecipeCraft.Serializer::new);
    public static final RegistryObject<PouchRecipeRepair.Serializer> CRAFTING_POUCH_REPAIR = RECIPES.register("pouch_repairing", PouchRecipeRepair.Serializer::new);
    public static final RegistryObject<PouchRecipeUpgrade.Serializer> CRAFTING_POUCH_UPGRADE = RECIPES.register("pouch_upgrading", PouchRecipeUpgrade.Serializer::new);

    public static void init() {
        final IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();

        ITEMS.register(modBus);
        RECIPES.register(modBus);
    }

}
