package com.horriblenerd.foodpouches.recipes;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.horriblenerd.foodpouches.FoodPouchItem;
import com.horriblenerd.foodpouches.Registration;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.SpecialRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class PouchRecipeRepair extends SpecialRecipe {
    public PouchRecipeRepair(ResourceLocation idIn) {
        super(idIn);
    }

    /**
     * Used to check if a recipe matches current crafting inventory
     */
    public boolean matches(CraftingInventory inv, World worldIn) {
        List<ItemStack> list = Lists.newArrayList();

        for (int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack itemstack = inv.getStackInSlot(i);
            if (!itemstack.isEmpty()) {
                if (itemstack.getItem() instanceof FoodPouchItem) {
                    list.add(itemstack);
                } else
                    return false;
            }
        }
        return list.size() >= 2;
    }

    /**
     * Returns an Item that is the result of this recipe
     */
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ArrayList<ItemStack> stackList = new ArrayList<>();

        ItemStack highest = ItemStack.EMPTY;
        int amount = 0;
        for (int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack stack = inv.getStackInSlot(i);
            if (!stack.isEmpty()) {
                if (stack.getItem() instanceof FoodPouchItem) {
                    stackList.add(stack);
                    if (stack.getMaxDamage() > amount || (stack.getMaxDamage() == amount && ((FoodPouchItem) stack.getItem()).isAutomatic())) {
                        amount = stack.getMaxDamage();
                        highest = stack.copy();
                    }
                }
            }
        }

        if (stackList.size() > 1) {
            int foodLevel = 0;
            for (ItemStack stack : stackList) {
                foodLevel += ((FoodPouchItem) stack.getItem()).getFoodLevel(stack);
            }
            ((FoodPouchItem) highest.getItem()).setFoodLevel(highest, foodLevel);
        }
        return highest;
    }

    /**
     * Used to determine if this recipe can fit in a grid of the given width/height
     */
    public boolean canFit(int width, int height) {
        return width * height >= 2;
    }

    public IRecipeSerializer<?> getSerializer() {
        return Registration.CRAFTING_POUCH_REPAIR.get();
    }

    public static class Serializer extends net.minecraftforge.registries.ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<PouchRecipeRepair> {
        private static final ResourceLocation NAME = new ResourceLocation("foodpouches", "pouch_repairing");

        public PouchRecipeRepair read(ResourceLocation recipeId, JsonObject json) {
            return new PouchRecipeRepair(recipeId);
        }

        public PouchRecipeRepair read(ResourceLocation recipeId, PacketBuffer buffer) {
            return new PouchRecipeRepair(recipeId);
        }

        public void write(PacketBuffer buffer, PouchRecipeRepair recipe) {
        }
    }
}